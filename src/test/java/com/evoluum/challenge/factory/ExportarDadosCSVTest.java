package com.evoluum.challenge.factory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.evoluum.challenge.domain.Estado;
import com.evoluum.challenge.domain.Mesorregiao;
import com.evoluum.challenge.domain.Microrregiao;
import com.evoluum.challenge.domain.Municipio;
import com.evoluum.challenge.domain.Regiao;
import com.evoluum.challenge.dto.CustomIBGEInfo;
import com.evoluum.challenge.exceptions.EstadoNaoEncontradoException;
import com.evoluum.challenge.exceptions.InvalidDataException;
import com.evoluum.challenge.exceptions.MunicipioNaoEncontradoException;
import com.evoluum.challenge.factory.ExportarDadosFactory;
import com.evoluum.challenge.intercomm.IIBGEIntercomm;

@RunWith(MockitoJUnitRunner.class)
public class ExportarDadosCSVTest {

	private CustomIBGEInfo customIBGEInfo;

	private List<CustomIBGEInfo> customList;
	
	@InjectMocks
	@Spy
	private ExportarDadosCSV exportarDadosFactory;
	
	
	@Before
	public void setup() {

		customIBGEInfo = new CustomIBGEInfo();
		
		customList = new ArrayList<>();
	}

	@Test
	public void exportarDadosTestSucess() {

		customIBGEInfo.setIdEstado("42");
		customIBGEInfo.setNomeCidade("São Miguel do Oeste");
		customIBGEInfo.setNomeFormatado("São Miguel do Oeste/SC");
		customIBGEInfo.setNomeMesorregiao("Oeste Catarinense");
		customIBGEInfo.setRegiaoNome("Sul");
		customIBGEInfo.setSiglaEstado("SC");
		
		customList.add(customIBGEInfo);
		
		String cabecalho = "idEstado;siglaEstado;regiaoNome;nomeCidade;nomeMesorregiao;nomeFormatado\n" + 
				"42;SC;Sul;São Miguel do Oeste;Oeste Catarinense;São Miguel do Oeste/SC\n";
		
		assertEquals(cabecalho, exportarDadosFactory.exportarDados(customList));

	}

}
