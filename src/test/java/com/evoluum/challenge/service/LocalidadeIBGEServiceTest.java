package com.evoluum.challenge.service;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.evoluum.challenge.domain.Estado;
import com.evoluum.challenge.domain.Mesorregiao;
import com.evoluum.challenge.domain.Microrregiao;
import com.evoluum.challenge.domain.Municipio;
import com.evoluum.challenge.domain.Regiao;
import com.evoluum.challenge.dto.CustomIBGEInfo;
import com.evoluum.challenge.exceptions.EstadoNaoEncontradoException;
import com.evoluum.challenge.exceptions.InvalidDataException;
import com.evoluum.challenge.exceptions.MunicipioNaoEncontradoException;
import com.evoluum.challenge.factory.ExportarDadosFactory;
import com.evoluum.challenge.intercomm.IIBGEIntercomm;

@RunWith(MockitoJUnitRunner.class)
public class LocalidadeIBGEServiceTest {

	@Mock
	private IIBGEIntercomm iBGEIntercomm = mock(IIBGEIntercomm.class);

	@Mock
	HttpServletRequest mockedRequest = mock(HttpServletRequest.class);

	@Mock
	private ExportarDadosFactory exportarDadosFactory;

	@InjectMocks
	@Spy
	private LocalidadeIBGEService localidadeIBGEService;

	private List<Estado> listEstados;

	private List<CustomIBGEInfo> listIbgeCustomInfo;

	private List<Municipio> listMunicipio;

	private Estado estado;

	private CustomIBGEInfo customIBGEInfo;

	private Municipio municipio;

	private Regiao regiao;

	private Microrregiao microrregiao;

	@Before
	public void setup() {
		listEstados = new ArrayList<>();

		listIbgeCustomInfo = new ArrayList<>();

		listMunicipio = new ArrayList<>();

		estado = new Estado();

		customIBGEInfo = new CustomIBGEInfo();

		municipio = new Municipio();

		microrregiao = new Microrregiao();

		regiao = new Regiao();
	}

	@Test
	public void adquirirDadosJsonSucess() {

		regiao.setId("4");
		regiao.setNome("Sul");
		regiao.setSigla("S");

		estado.setId("42");
		estado.setNome("Santa Catarina");
		estado.setSigla("SC");
		estado.setRegiao(regiao);

		microrregiao.setId("42009");
		microrregiao.setNome("Curitibanos");
		microrregiao.setMesorregiao(new Mesorregiao());

		municipio.setId("4200051");
		municipio.setNome("Abdon Batista");
		municipio.setMicrorregiao(microrregiao);

		customIBGEInfo.setIdEstado("12");
		customIBGEInfo.setNomeCidade(municipio.getNome());

		listIbgeCustomInfo.add(customIBGEInfo);

		listEstados.add(estado);

		listMunicipio.add(municipio);

		when(iBGEIntercomm.adquirirEstados()).thenReturn(listEstados);

		when(iBGEIntercomm.adquirirMunicipiosPorEstado(estado.getSigla())).thenReturn(listMunicipio);

		assertEquals(customIBGEInfo.getNomeCidade(), localidadeIBGEService.adquirirDadosJson().get(0).getNomeCidade());

	}

	@Test
	public void adquirirIdMunicipioSucess() {

		municipio.setId("4200051");
		municipio.setNome("Abdon");
		municipio.setMicrorregiao(new Microrregiao());

		listMunicipio.add(municipio);

		when(iBGEIntercomm.adquirirMunicipios()).thenReturn(listMunicipio);

		assertEquals(municipio.getId(), localidadeIBGEService.adquirirIdCidade(municipio.getNome()).getIdCidade());

	}

	@Test
	public void adquirirIdMunicipioNaoEncontrado() {

		municipio.setId("4200051");
		municipio.setNome("Abdon");
		municipio.setMicrorregiao(new Microrregiao());

		when(iBGEIntercomm.adquirirMunicipios()).thenReturn(listMunicipio);

		String message = assertThrows(MunicipioNaoEncontradoException.class,
				() -> localidadeIBGEService.adquirirIdCidade("noName")).getMessage();

		assertEquals("Nenhum municipio não foi encontrado.", message);

	}

	
	@Test
	public void adquirirEstadosSemRetornoAPI() {

		

		when(iBGEIntercomm.adquirirEstados()).thenReturn(listEstados);

		
		String message = assertThrows(EstadoNaoEncontradoException.class,
				() -> localidadeIBGEService.adquirirDadosJson()).getMessage();

		assertEquals("Nenhum estado não foi encontrado.", message);

	}
	
	@Test
	public void adquirirIdMunicipioPesquisadoNaoEncontrado() {

		municipio.setId("4200051");
		municipio.setNome("Abdon");
		municipio.setMicrorregiao(new Microrregiao());

		listMunicipio.add(municipio);
		
		when(iBGEIntercomm.adquirirMunicipios()).thenReturn(listMunicipio);

		String message = assertThrows(MunicipioNaoEncontradoException.class,
				() -> localidadeIBGEService.adquirirIdCidade("noName")).getMessage();

		assertEquals("O municipio pesquisado não foi encontrado.", message);

	}
	
	@Test
	public void adquirirDadosCSVPassandoParamNull() {

		municipio.setId("4200051");
		municipio.setNome(null);
		municipio.setMicrorregiao(new Microrregiao());

		listMunicipio.add(municipio);

		when(iBGEIntercomm.adquirirMunicipios()).thenReturn(listMunicipio);

		String message = assertThrows(InvalidDataException.class,
				() -> localidadeIBGEService.adquirirIdCidade(municipio.getNome())).getMessage();

		assertEquals("O parâmetro não pode ser vazio.", message);

	}

}
