package com.evoluum.challenge.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ExternalRequestConfig {

	@Bean
	public RestTemplate externalRequest() {
		RestTemplate restTemplate = new RestTemplate();

		return restTemplate;

	}
}
