package com.evoluum.challenge.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.evoluum.challenge.exceptions.InvalidDataException;

@ControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(InvalidDataException.class)
	public ResponseEntity<JsonResponse> handleRunTimeException(Exception e, HttpServletRequest request) {

		return new ResponseEntity<JsonResponse>(new JsonResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	
	private class JsonResponse {
		String message;

		public JsonResponse(String message) {
			super();
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}

}