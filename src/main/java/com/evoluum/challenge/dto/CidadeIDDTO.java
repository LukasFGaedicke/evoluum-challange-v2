package com.evoluum.challenge.dto;

public class CidadeIDDTO {

	private String idCidade;

	public CidadeIDDTO(String idCidade) {
		this.idCidade = idCidade;
	}

	public String getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(String idCidade) {
		this.idCidade = idCidade;
	}

}
