package com.evoluum.challenge.domain;

import java.util.List;

public class Pais {

	private int id;

	private String nome;

	private List<UnidadeFederativa> UF;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<UnidadeFederativa> getUF() {
		return UF;
	}

	public void setUF(List<UnidadeFederativa> uF) {
		UF = uF;
	}

}
