package com.evoluum.challenge.exceptions;

public class CriarArquivoCSVException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CriarArquivoCSVException(String message) {
		super(message);
	}
}
