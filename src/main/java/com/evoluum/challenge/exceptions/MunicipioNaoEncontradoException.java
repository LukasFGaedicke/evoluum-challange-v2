package com.evoluum.challenge.exceptions;

public class MunicipioNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MunicipioNaoEncontradoException(String message) {
		super(message);
	}
}
