package com.evoluum.challenge.exceptions;

public class EstadoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EstadoNaoEncontradoException(String message) {
		super(message);
	}
}
