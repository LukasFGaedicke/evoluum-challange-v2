package com.evoluum.challenge.intercomm;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.evoluum.challenge.domain.Estado;
import com.evoluum.challenge.domain.Municipio;
import com.evoluum.challenge.service.LocalidadeIBGEService;

@Service
public class IBGEIntercomm implements IIBGEIntercomm{

	@Autowired
	private RestTemplate restTemplate;

	private static final Logger LOG = LoggerFactory.getLogger(LocalidadeIBGEService.class);

	public static String url_municipios_custom = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/{UF}/municipios";

	public static String url_municipios = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios";

	public static String url_estados = "https://servicodados.ibge.gov.br/api/v1/localidades/estados";

	public List<Estado> adquirirEstados() {

		LOG.info("Requisição para buscar lista de estados enviada...");

		return Arrays.asList(restTemplate.getForEntity(url_estados, Estado[].class).getBody());

	}

	public List<Municipio> adquirirMunicipiosPorEstado(String siglaEstado) {
		String customUrl = url_municipios_custom.replace("{UF}", siglaEstado);

		LOG.info("Requisição para buscar lista de municipios do estado " + siglaEstado + " enviada...");

		return Arrays.asList(restTemplate.getForEntity(customUrl, Municipio[].class).getBody());

	}

	public List<Municipio> adquirirMunicipios() {

		LOG.info("Requisição para buscar municipios enviada...");
		
		return Arrays.asList(restTemplate.getForEntity(url_municipios, Municipio[].class).getBody());

	}

}
