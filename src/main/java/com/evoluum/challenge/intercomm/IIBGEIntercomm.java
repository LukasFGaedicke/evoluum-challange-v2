package com.evoluum.challenge.intercomm;

import java.util.List;
import com.evoluum.challenge.domain.Estado;
import com.evoluum.challenge.domain.Municipio;

public interface IIBGEIntercomm {

	public abstract List<Estado> adquirirEstados();

	public List<Municipio> adquirirMunicipiosPorEstado(String siglaEstado);

	public abstract List<Municipio> adquirirMunicipios();
}
