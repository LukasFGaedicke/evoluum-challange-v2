package com.evoluum.challenge.resource;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.evoluum.challenge.dto.CidadeIDDTO;
import com.evoluum.challenge.dto.CustomIBGEInfo;
import com.evoluum.challenge.service.LocalidadeIBGEService;

@RestController
@RequestMapping()
public class LocalidadeIBGE {

	@Autowired
	private LocalidadeIBGEService localidadeIBGEService;
	
	private static final Logger LOG = LoggerFactory.getLogger(LocalidadeIBGE.class);
	
	@GetMapping("/json")
	public ResponseEntity<List<CustomIBGEInfo>> adquirirJSON(HttpServletResponse response) {

		return ResponseEntity.ok(localidadeIBGEService.adquirirDadosJson());

	}

	@GetMapping("/csv")
	public HttpServletResponse adquirirCSV(HttpServletResponse response) {

		return localidadeIBGEService.adquirirDadosCSV(response);

	}

	@GetMapping("buscarIdMunicipio/{nomeMunicipio}")
	public ResponseEntity<CidadeIDDTO> adquirirIdMunicipio(@PathVariable String nomeMunicipio) {

		return ResponseEntity.ok(localidadeIBGEService.adquirirIdCidade(nomeMunicipio));
	}

}
