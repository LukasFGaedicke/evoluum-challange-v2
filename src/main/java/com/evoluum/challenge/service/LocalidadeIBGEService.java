package com.evoluum.challenge.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.evoluum.challenge.domain.Estado;
import com.evoluum.challenge.domain.Municipio;
import com.evoluum.challenge.dto.CidadeIDDTO;
import com.evoluum.challenge.dto.CustomIBGEInfo;
import com.evoluum.challenge.exceptions.MunicipioNaoEncontradoException;
import com.evoluum.challenge.exceptions.CriarArquivoCSVException;
import com.evoluum.challenge.exceptions.EstadoNaoEncontradoException;
import com.evoluum.challenge.exceptions.InvalidDataException;
import com.evoluum.challenge.factory.ExportarDadosCSV;
import com.evoluum.challenge.factory.ExportarDadosFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class LocalidadeIBGEService implements ILocalidadeIBGEService {

	private static final Logger LOG = LoggerFactory.getLogger(LocalidadeIBGEService.class);

	@Autowired
	private ExportarDadosFactory exportarDadosFactory;

	@Autowired
	private com.evoluum.challenge.intercomm.IIBGEIntercomm IBGEIntercomm;

	private final String NOME_CIDADE_CACHE = "cidadeCache";

	private List<Estado> adquirirEstados() {
		List<Estado> retornoEstados = IBGEIntercomm.adquirirEstados();

		if (retornoEstados.isEmpty()) {

			throw new EstadoNaoEncontradoException("Nenhum estado não foi encontrado.");
		}

		return retornoEstados;
	}

	private List<CustomIBGEInfo> adquirirInformacaoesCustomIBGE() {

		List<Estado> retornoEstados = this.adquirirEstados();

		List<CustomIBGEInfo> ibgeCustomInfo = new ArrayList<>();

		LOG.info("Processando dados para criar os arquivo(s)...");

		retornoEstados.forEach(estado -> {

			IBGEIntercomm.adquirirMunicipiosPorEstado(estado.getSigla()).forEach(municipio -> {
				ibgeCustomInfo.add(new CustomIBGEInfo(estado.getId(), estado.getSigla(), estado.getRegiao().getNome(),
						municipio.getNome(), municipio.getMicrorregiao().getNome(),
						municipio.getNome().concat("/" + estado.getSigla())));
			});

		});

		LOG.info("Dados processados com sucesso...");

		return ibgeCustomInfo;
	}

	public List<CustomIBGEInfo> adquirirDadosJson() {

		return this.adquirirInformacaoesCustomIBGE();
	}

	public HttpServletResponse adquirirDadosCSV(HttpServletResponse response) {
		exportarDadosFactory = new ExportarDadosCSV();

		String csvMontado = exportarDadosFactory.exportarDados(this.adquirirInformacaoesCustomIBGE());

		return editarResponse(response, csvMontado);
	}

	@Cacheable(value = NOME_CIDADE_CACHE)
	public CidadeIDDTO adquirirIdCidade(String nomeCidade) {

		this.validarCampoCidade(nomeCidade);

		List<Municipio> municipios = IBGEIntercomm.adquirirMunicipios();

		if (municipios.isEmpty()) {
			throw new MunicipioNaoEncontradoException("Nenhum municipio não foi encontrado.");
		}

		LOG.info("Buscando ID do municipio informado...");

		for (Municipio municipio : municipios) {

			if (formatarParamEntrada(municipio.getNome()).equals(formatarParamEntrada(nomeCidade))) {

				LOG.info("ID do municipio foi encontrado...");

				return new CidadeIDDTO(municipio.getId());
			}
		}

		throw new MunicipioNaoEncontradoException("O municipio pesquisado não foi encontrado.");

	}

	private String formatarParamEntrada(String param) {

		String semAcentos = param.replaceAll("[~^]*", " ");

		String semEspaco = semAcentos.replaceAll("\\s+", "");

		return semEspaco.toLowerCase();
	}

	private void validarCampoCidade(String nomeCidade) {

		LOG.info("Validando parametros...");

		if (nomeCidade == null || nomeCidade.length() < 1) {

			throw new InvalidDataException("O parâmetro não pode ser vazio.");
		}

	}

	private HttpServletResponse editarResponse(HttpServletResponse response, String csvMontado) {

		String currentDate = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());

		response.addHeader("Content-disposition", "attachment;filename=IBGE-Dados-".concat(currentDate + ".csv"));

		try {

			response.getOutputStream().write(csvMontado.getBytes());
			response.flushBuffer();

		} catch (IOException e) {

			throw new CriarArquivoCSVException("Ocorreu algum erro ao criar o arquivo CSV" + e.getMessage());

		}

		return response;
	}

}
