package com.evoluum.challenge.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.evoluum.challenge.dto.CidadeIDDTO;
import com.evoluum.challenge.dto.CustomIBGEInfo;

public interface ILocalidadeIBGEService {

	public abstract List<CustomIBGEInfo> adquirirDadosJson();

	public abstract HttpServletResponse adquirirDadosCSV(HttpServletResponse response);

	public abstract CidadeIDDTO adquirirIdCidade(String nomeCidade);

}
