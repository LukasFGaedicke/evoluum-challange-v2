package com.evoluum.challenge.factory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import com.evoluum.challenge.dto.CustomIBGEInfo;

public interface ExportarDadosFactory {

	public abstract String exportarDados(List<CustomIBGEInfo> listaEstados);
}
