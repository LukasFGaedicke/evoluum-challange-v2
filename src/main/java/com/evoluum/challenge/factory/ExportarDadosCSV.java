package com.evoluum.challenge.factory;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.evoluum.challenge.dto.CustomIBGEInfo;
import com.evoluum.challenge.service.LocalidadeIBGEService;

@Service
public class ExportarDadosCSV implements ExportarDadosFactory {
	
	private static final Logger LOG = LoggerFactory.getLogger(LocalidadeIBGEService.class);
	
	private final String NOMES_COLUNAS = "idEstado;siglaEstado;regiaoNome;nomeCidade;nomeMesorregiao;nomeFormatado";
	private final String NOVA_LINHA = "\n";

	@Override
	public String exportarDados(List<CustomIBGEInfo> listaEstados) {

		LOG.info("Criando arquivo CSV com os dados...");

		StringBuilder conteudoCSV = new StringBuilder();

		conteudoCSV.append(NOMES_COLUNAS + NOVA_LINHA);

		for (CustomIBGEInfo value : listaEstados) {
			conteudoCSV.append(value.toStringWithSeparator()).append(NOVA_LINHA);
		}

		LOG.info("Arquivo CSV com os dados criado com sucesso...");

		return conteudoCSV.toString();

	}

}
